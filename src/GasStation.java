import java.util.Random;

public class GasStation {

    static GasTank gasTank = new GasTank();
    static FuelPump fuelPump = new FuelPump();

    //testing
    public static void main(String[] args) {

        // This should print out 1000
        System.out.println("Current capacity: "+gasTank.getCurrentAmount());

        // This should set the current capacity to 800
        fuelPump.pumpFuel(gasTank, 200f);

        // This should print out 800
        System.out.println("Current capacity: "+gasTank.getCurrentAmount());

        // This should print out 900
        gasTank.addFuel(100f);
        System.out.println("Current capacity: "+gasTank.getCurrentAmount());

        // This should print out Gas tank filled to max capacity and then 1000
        gasTank.addFuel(2000f);
        System.out.println("Current capacity: "+gasTank.getCurrentAmount());

        // This should print out 0
        fuelPump.pumpFuel(gasTank,1000f);
        System.out.println("Current capacity: "+gasTank.getCurrentAmount());

        // This should print out the turnover
        System.out.println(fuelPump.getTurnOver(100));

    }
}
