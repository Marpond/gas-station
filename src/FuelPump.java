public class FuelPump {

    // Variable
    private float counter;

    // Default constructor
    public FuelPump() {
        this.counter = 0;
    }

    /**
     * This is to pump the fuel
     * @param gasTank The gas tank you wanna use, if there's more than one
     * @param amount The amount of fuel you wanna pump from the gas tank
     */
    public void pumpFuel(GasTank gasTank, float amount) {

        if (gasTank.getCurrentAmount() != 0){
            if (gasTank.canPump(amount)){
                this.counter += amount;
                gasTank.removeFuel(amount);
            } else {
                this.counter += gasTank.getCurrentAmount();
                gasTank.setCurrentAmount(0f);
            }
        } else {
            System.out.print("Gas tank is empty.\n");
        }
    }

    // Extra task - Method

    /**
     * This is to get the turnover from the fuel pump
     * @param price The price per liter
     * @return How much the fuel pump has earned
     */
    public int getTurnOver(int price) {
        return (int) (price * counter);
    }
}
