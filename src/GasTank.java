public class GasTank {

    private float currentAmount;
    private final float MAXCAPACITY = 1000f;

    //default constructor
    public GasTank() {
        this.currentAmount = this.MAXCAPACITY;
    }

    /**
     * Get the current amount of the gas tank
     * @return The amount of fuel that gas tank has
     */
    public float getCurrentAmount() {
        return this.currentAmount;
    }

    /**
     * This is to set the amount of fuel in the tank
     * @param amount The amount the gas tank should have
     */
    public void setCurrentAmount(float amount) {
        this.currentAmount = amount;
    }

    /**
     * This is to add fuel to the tank
     * @param amount The amount of fuel you want to add
     */
    public void addFuel(float amount) {

        if (this.currentAmount + amount >= this.MAXCAPACITY) {
            // Max out the gas tank
            this.currentAmount = this.MAXCAPACITY;
            System.out.print("Gas tank filled to max capacity\n");
        } else {
            // Add the amount to the gas tank
            this.currentAmount += amount;
            System.out.printf("Added %f fuel to gas tank\n",amount);
        }

    }

    /**
     * Removes certain amount of fuel from gasTank
     * Checks whether gasTank is empty
     * @param amount The amount it'll remove
     */
    public void removeFuel(float amount) {
        if (this.currentAmount == 0) {
            System.out.print("Gas tank is empty");
        } else {
            if (this.currentAmount - amount <= 0) {
                this.currentAmount = 0f;
                System.out.print("Emptied the tank");
            } else {
                this.currentAmount -= amount;
                System.out.printf("Removed %f liters from the tank.",amount);
            }
        }
    }

    /**
     * This is to check if you can pump fuel
     * @param amount The amount of fuel you wanna pump
     * @return whether or not you can pump that amount of fuel
     */
    public boolean canPump(float amount) {

        return amount <= this.currentAmount;

    }

}
